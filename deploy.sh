source .env
tar -czvf build.tar.gz build
scp build.tar.gz $MASTER_SERVER_URL:~
ssh $MASTER_SERVER_URL './unpack.sh'
rm build.tar.gz
