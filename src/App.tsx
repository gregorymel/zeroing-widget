import React from "react";
import "./App.css";
import Exchange from "./components/exchange/Exchange";
import { useMetaMask } from "./hooks/useMetaMask";
import { Button } from "antd";

function App() {
  const { account } = useMetaMask();

  return (
    <div className="App">
      <Exchange />
    </div>
  );
}

export default App;
