import React from "react";
import { Form, Input, Select } from "antd";

type ExchangeInputType = "give" | "take";
export type Currency = "ETH" | "DAI";
const allCurrencies: Record<Currency, boolean> = {
  ETH: true,
  DAI: true,
};

const labels: Record<ExchangeInputType, string> = {
  give: "Получить",
  take: "Отдать",
};

interface ICurrencySelect {
  type: ExchangeInputType;
  selectedCurrency?: Currency;
}
const CurrencySelect: React.FC<ICurrencySelect> = (props) => {
  const { type, selectedCurrency } = props;
  return (
    <Form.Item name={`currency-${type}`} noStyle>
      <Select value={selectedCurrency}>
        {Object.entries(allCurrencies).map(([currency]) => (
          <Select.Option
            key={currency}
            value={currency}
            disabled={currency === selectedCurrency}
          >
            {currency}
          </Select.Option>
        ))}
      </Select>
    </Form.Item>
  );
};

interface IExchangeInput {
  type: ExchangeInputType;
  disabled?: boolean;
}
const ExchangeInput: React.FC<IExchangeInput> = (props) => {
  const { type, disabled } = props;

  return (
    <Form.Item name={`currency-${type}-value`} noStyle>
      <Input
        addonBefore={labels[type]}
        addonAfter={<CurrencySelect type={type} />}
        type={"number"}
        disabled={disabled}
      />
    </Form.Item>
  );
};

export default ExchangeInput;
