import React from "react";
import ExchangeInput from "./ExchangeInput";
import "./Exchange.scss";
import {
  Button,
  Col,
  Form,
  FormProps,
  message,
  Row,
  Space,
  Typography,
} from "antd";
import Info from "./Info";
import { exchangeService } from "../../services/exchangeService";
import { metaMaskService } from "../../services/metaMaskService";
import { useMetaMask } from "../../hooks/useMetaMask";
import { InfoCircleOutlined } from "@ant-design/icons";

const { Text } = Typography;

interface IExchange {}

const Exchange: React.FC<IExchange> = (props) => {
  const [loading, setLoading] = React.useState<boolean>(false);
  const [form] = Form.useForm();
  const { account } = useMetaMask();

  const onConfirm: FormProps["onFinish"] = async (data) => {
    if (!data["currency-give-value"]) {
      message.error("Пожалуйста введите кол-во валюты, которую хотите отдать");
      return;
    }
    if (!data["currency-take-value"]) {
      message.error(
        "Пожалуйста введите кол-во валюты, которую хотите получить"
      );
      return;
    }

    console.log(data);
    setLoading(true);
    const result = await exchangeService.transact({
      input_token: "eth",
      output_token: "0x6b175474e89094c44da98b954eedeac495271d0f",
      amount: data["currency-take-value"] * 10 ** 18,
      from: account,
    });
    setLoading(false);
  };
  const onChange: FormProps["onFieldsChange"] = async (changedData, data) => {
    function getData(givenName: string) {
      return form.getFieldValue([givenName]);
    }

    function updateInputValue<T extends string>(
      givenName: string,
      value?: string
    ) {
      form.setFieldsValue({
        [givenName]: value,
      });
    }
    async function updateVersusCurrency() {
      const price = await exchangeService.checkCurrencies(
        getData("currency-take"),
        getData("currency-give")
      );

      let tmp = changedData.find(
        // @ts-ignore
        (v) => v.name[0] === "currency-give-value"
      );
      if (tmp) {
        updateInputValue(
          "currency-take-value",
          ((1 / price) * Number(getData("currency-give-value"))).toFixed(2)
        );
      }

      tmp = changedData.find(
        // @ts-ignore
        (v) => v.name[0] === "currency-take-value"
      );
      if (tmp) {
        updateInputValue(
          "currency-give-value",
          (price * Number(getData("currency-take-value"))).toFixed(2)
        );
      }

      tmp = changedData.find(
        // @ts-ignore
        (v) => ["currency-give", "currency-take"].includes(v.name[0])
      );

      if (tmp) {
        form.setFieldsValue({
          "currency-take-value": 0,
          "currency-give-value": 0,
        });
      }
    }

    if (!getData("currency-take-value")) {
      if (getData("currency-give-value")) {
      } else {
        // setGiveDisabled(false);
        // setTakeDisabled(false);
      }
    } else {
      if (getData("currency-give-value")) {
      } else {
        // setGiveDisabled(true);
        // setTakeDisabled(false);
      }
    }

    updateVersusCurrency();
  };

  const getMetaMask = async () => {
    const account = await metaMaskService.getCurrentAccount();
    console.log(account);
  };

  return (
    <Form
      id={"exchange"}
      onFinish={onConfirm}
      onFieldsChange={onChange}
      form={form}
      initialValues={{
        "currency-take": "ETH",
        "currency-give": "DAI",
      }}
    >
      <div className={"sb-exchange"}>
        <Space direction={"vertical"} size={"middle"}>
          <Row>
            <Col span={24} className={"sb-exchange__account"}>
              {account ? (
                <Text
                  ellipsis={{
                    tooltip: account,
                  }}
                >
                  Аккаунт в MetaMask:&nbsp;<b>{account.slice(0, 10)}...</b>
                </Text>
              ) : (
                <Button type={"primary"} onClick={getMetaMask}>
                  Подключить MetaMask
                </Button>
              )}
            </Col>
          </Row>
          <Row>
            <Col span={24}>
              <ExchangeInput type={"take"} />
              <ExchangeInput type={"give"} />
            </Col>
          </Row>
          <Row>
            <Col span={24}>
              <Info
                values={
                  [
                    // {
                    //   value: "0",
                    //   name: "Получите минимум",
                    // },
                  ]
                }
              />
            </Col>
          </Row>
          <Row justify={"center"}>
            <Button
              type="primary"
              htmlType={"submit"}
              loading={loading}
              size={"large"}
            >
              Обменять
            </Button>
          </Row>
          <Row className={"sb-exchange__verified-button"}>
            <Button
              type={"link"}
              href={
                "https://app.zerion.io/invest/asset/0x6b175474e89094c44da98b954eedeac495271d0f"
              }
              icon={<InfoCircleOutlined />}
              rel={"noreferrer"}
              target={"_blank"}
            >
              Токен верифицирован в нескольких списках
            </Button>
          </Row>
        </Space>
      </div>
    </Form>
  );
};

export default Exchange;
