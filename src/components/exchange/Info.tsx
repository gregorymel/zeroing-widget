import React from "react";
import { Col, Row, Typography } from "antd";
import "./Info.scss";

type ValueType = "willGet";

const { Text } = Typography;

interface IValue {
  name: string;
  value: string;
}
const Value: React.FC<IValue> = (props) => {
  const { value, name } = props;

  return (
    <Row>
      <Col span={12}>
        <Text>{name}</Text>
      </Col>
      <Col span={12}>
        <Row justify={"end"}>
          <Text>{value}</Text>
        </Row>
      </Col>
    </Row>
  );
};

interface IInfo {
  values: IValue[];
}
const Info: React.FC<IInfo> = (props) => {
  const { values } = props;

  return (
    <>
      <div className={"sb-exchange-info"}>
        {values.map(({ value, name }) => (
          <Value name={name} value={value} key={name} />
        ))}
      </div>
    </>
  );
};

export default Info;
