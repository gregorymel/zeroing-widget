import React, { useEffect } from "react";
import { metaMaskService } from "../services/metaMaskService";

export const useMetaMask = () => {
  const [account, setAccount] = React.useState<string | null>(
    metaMaskService._account
  );

  const asyncAction = React.useCallback(async () => {
    const obtainedAccount = await metaMaskService.getCurrentAccount();
    setAccount(obtainedAccount);
  }, []);

  useEffect(() => {
    asyncAction();
  }, []);

  return {
    account,
  };
};
