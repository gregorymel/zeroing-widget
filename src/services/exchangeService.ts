import { metaMaskService } from "./metaMaskService";
import { Currency } from "../components/exchange/ExchangeInput";
import axios from "axios";
import { message } from "antd";

export const currencyMap: Record<Currency, string> = {
  ETH: "ethereum",
  DAI: "usd",
};

type InfoInstance = {
  value: number;
  lastUpdated: Date;
};
type Info = {
  prices: Partial<Record<Currency, Partial<Record<Currency, InfoInstance>>>>;
};

const mockTransactResponse = {
  contract_metadata: {
    id: "string",
    name: "string",
    icon_url: "",
  },
  slippage_type: "normal",
  output_amount: "string",
  guaranteed_output_amount: "string",
  distribution: {
    additionalProp1: {
      share: 0,
      icon_url: "",
    },
    additionalProp2: {
      share: 0,
      icon_url: "",
    },
    additionalProp3: {
      share: 0,
      icon_url: "",
    },
  },
  token_spender: "string",
  enough_allowance: false,
  estimated_gas: 0,
  transaction: {
    to: "0x0000000000000000000000000000000000000000",
    from: "0x0000000000000000000000000000000000000000",
    chain_id: "0x1",
    gas: 0,
    data: "0x",
    value: "0",
    protocol: "string",
  },
};

class ExchangeService {
  _lastInfo: Info = {
    prices: {},
  };
  _currenciesInfoUrl(currency: Currency, versusCurrency: Currency): string {
    let ids = currencyMap[currency];
    let vs_currencies = currencyMap[versusCurrency];

    if (ids === "usd") {
      const tmp = ids;
      ids = vs_currencies;
      vs_currencies = tmp;
    }

    return `https://api.coingecko.com/api/v3/simple/price?ids=${ids}&vs_currencies=${vs_currencies}`;
  }

  async checkCurrencies(
    currency: Currency,
    versusCurrency: Currency
  ): Promise<number> {
    const func = async (currency: Currency, versusCurrency: Currency) => {
      const { data } = await axios.get(
        this._currenciesInfoUrl(currency, versusCurrency)
      );

      return data[currencyMap[currency]][currencyMap[versusCurrency]];
    };

    if (!this._lastInfo.prices[currency]) {
      const value = await func(currency, versusCurrency);

      this._lastInfo.prices[currency] = {
        [versusCurrency]: {
          value,
          lastUpdated: new Date(),
        },
      };
    } else {
      if (!this._lastInfo.prices[currency]![versusCurrency]) {
        const value = await func(currency, versusCurrency);

        this._lastInfo.prices[currency]![versusCurrency] = {
          value,
          lastUpdated: new Date(),
        };
      } else {
        const { lastUpdated } =
          this._lastInfo.prices[currency]![versusCurrency]!;

        if (Number(new Date()) - Number(lastUpdated) >= 1000) {
          const value = await func(currency, versusCurrency);

          this._lastInfo.prices[currency]![versusCurrency] = {
            value,
            lastUpdated: new Date(),
          };
        }
      }
    }

    return this._lastInfo.prices[currency]![versusCurrency]!.value;
  }

  async _update() {}

  async transact(params: any) {
    try {
      const { data } = await axios.get(
        "https://transactions.zerion.io/swap/transact",
        {
          params,
        }
      );
      await metaMaskService.performTransaction(data.transaction, true);
    } catch (e: any) {
      message.error(e.response?.data.body);
    }
  }
}

export const exchangeService = new ExchangeService();
