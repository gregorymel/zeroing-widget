declare let window: any;

import Web3 from "web3";

type CurrentAccountType = string | null;

class MetaMaskService {
  _account: CurrentAccountType = null;
  _web3 = new Web3(window.ethereum);

  async _init() {
    window.addEventListener("load", async () => {
      // Wait for loading completion to avoid race conditions with web3 injection timing.
      if (window.ethereum) {
        const web3 = new Web3(window.ethereum);
        try {
          // Request account access if needed
          await window.ethereum.enable();
          // Acccounts now exposed
          this._web3 = web3;
        } catch (error) {
          console.error(error);
        }
      }
      // Legacy dapp browsers...
      else if (window.web3) {
        // Use Mist/MetaMask's provider.
        this._web3 = window.web3;
      }
      // Fallback to localhost; use dev console port by default...
      else {
        const provider = new Web3.providers.HttpProvider(
          "http://127.0.0.1:9545"
        );
        const web3 = new Web3(provider);
        console.log("No web3 instance injected, using Local web3.");
        this._web3 = web3;
      }
    });
  }

  constructor() {
    this._init();
    this._update();
  }

  async _update() {
    const accounts = await window.ethereum.request({
      method: "eth_requestAccounts",
    });
    this._account = accounts[0];
  }

  async getCurrentAccount(): Promise<CurrentAccountType> {
    await this._update();
    return this._account;
  }

  async performTransaction(params: any, useWeb3 = true) {
    if (useWeb3) {
      this._web3.eth.sendTransaction(params);
    } else {
      await window.ethereum.request({
        method: "eth_sendTransaction",
        params: [params],
      });
    }
  }
}

export const metaMaskService = new MetaMaskService();
